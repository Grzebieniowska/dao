CREATE USER 'hbstudent'@'localhost' IDENTIFIED BY 'hbstudent';
GRANT ALL PRIVILEGES ON * . * TO 'hbstudent'@'localhost';

DROP SCHEMA IF EXISTS hibernateclass;
CREATE SCHEMA hibernateclass;
USE hibernateclass;

SET FOREIGN_KEY_CHECKS = 0;
SET FOREIGN_KEY_CHECKS = 1;

# drop all tables

SELECT CONCAT( "DROP TABLE ", GROUP_CONCAT(TABLE_NAME) ) INTO @dropcmd FROM information_schema.TABLES WHERE TABLE_SCHEMA = "hibernateclass";
PREPARE str FROM @dropcmd; 
EXECUTE str; 
DEALLOCATE PREPARE str;

# create tables
CREATE TABLE instructor_detail (
  id int(11) NOT NULL AUTO_INCREMENT,
  youtube_channel varchar(128) DEFAULT NULL,
  hobby varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE instructor (
  id int(11) NOT NULL AUTO_INCREMENT,
  first_name varchar(45) DEFAULT NULL,
  last_name varchar(45) DEFAULT NULL,
  email varchar(45) DEFAULT NULL,
  instructor_detail_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK_DETAIL_idx (instructor_detail_id),
  CONSTRAINT FK_DETAIL FOREIGN KEY (instructor_detail_id) REFERENCES instructor_detail (id) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE course (
  id int(11) NOT NULL AUTO_INCREMENT,
  title varchar(45) DEFAULT NULL,
  instructor_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY TITLE_UNIQUE (title),
  KEY FK_INSTRUCTOR_idx (instructor_id),
  CONSTRAINT FK_INSTRUCTOR FOREIGN KEY (instructor_id) REFERENCES instructor (id) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE review (
id INT(11) NOT NULL AUTO_INCREMENT,
content VARCHAR(256),
course_id INT(11),
PRIMARY KEY (id),
KEY FK_COURSE_idx (course_id),
CONSTRAINT FK_COURSE FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE student (
  id int(11) NOT NULL AUTO_INCREMENT,
  first_name varchar(45) DEFAULT NULL,
  last_name varchar(45) DEFAULT NULL,
  email varchar(45) DEFAULT NULL,
  date_of_birth DATETIME NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;

#Join table:

CREATE TABLE course_student (
course_id INT(11),
student_id INT(11),
PRIMARY KEY (course_id,student_id),
  KEY FK_STUDENT_idx (student_id),
  CONSTRAINT FK_COURSE_05 FOREIGN KEY (course_id) 
  REFERENCES course (id) 
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FK_STUDENT FOREIGN KEY (student_id) 
  REFERENCES student (id) 
  ON DELETE NO ACTION ON UPDATE NO ACTION

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;