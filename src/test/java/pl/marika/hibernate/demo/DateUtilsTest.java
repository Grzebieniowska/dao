package pl.marika.hibernate.demo;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import pl.marika.hibernate.utils.DateUtils;

import java.text.ParseException;
import java.time.LocalDate;

/**
 * @author Marika Grzebieniowska on 23.07.2018
 * @project hibernate_tutorial
 */
public class DateUtilsTest {

    @Test
    public void shouldParseStringToLocalDate() throws ParseException {
        // if
        String date = "1982/11/03";

        // when
        LocalDate localDate = DateUtils.parseStringToLocalDate(date);

        // then
        Assertions.assertThat(localDate.equals(LocalDate.of(1982, 11, 3)));
    }

    @Test
    public void parseLocalDateToString() throws ParseException {
        // if
        LocalDate localDate = LocalDate.of(2000, 2, 12);

        // when
        String stringFromLocalDate = DateUtils.parseLocalDateToString(localDate);

        // then
        Assertions.assertThat(stringFromLocalDate.equals("2000/02/12"));

    }


}