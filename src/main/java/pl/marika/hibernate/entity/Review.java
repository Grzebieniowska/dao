package pl.marika.hibernate.entity;

import javax.persistence.*;

/**
 * @author Marika Grzebieniowska on 25.07.2018
 * @project hibernate_tutorial
 */
@Entity
@Table(name = "review")
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "content")
    private String content;

    // no relation info to Course, because we will never need to
    // search course by review. Review doesn't need to know about Course,
    // only the other way around.

    public Review() {
    }

    public Review(String content) {
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", content='" + content + '\'' +
                '}';
    }
}
