package pl.marika.hibernate.utils;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Marika Grzebieniowska on 23.07.2018
 * @project hibernate_tutorial
 */
public class DateUtils {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

    public static LocalDate parseStringToLocalDate(String dateStr) throws ParseException {
        return LocalDate.parse(dateStr, formatter);
    }

    public static String parseLocalDateToString(LocalDate localDate) throws ParseException{
        return localDate == null ? null : localDate.format(formatter);
    }
}
