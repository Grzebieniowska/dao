package pl.marika.hibernate.demo.CRUD;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Student;

/**
 * @author Marika Grzebieniowska on 22.07.2018
 * @project hibernate_tutorial
 */
public class ReadStudent {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session;

        try {
            session = factory.getCurrentSession();
            session.beginTransaction(); // in Hibernate even read actions on DB are transactions
            Student studentFromDb = session.get(Student.class, 1);
            session.getTransaction().commit();
        } finally {
            factory.close();
        }
    }
}
