package pl.marika.hibernate.demo.CRUD;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Student;

import java.util.List;

/**
 * @author Marika Grzebieniowska on 23.07.2018
 * @project hibernate_tutorial
 */
public class QueryStudent {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session;

        try {

            session = factory.getCurrentSession();
            session.beginTransaction();

            List<Student> studentsFromDb = session.createQuery("FROM Student").getResultList();

            List<Student> studentsFromDbFindByLastName = session.createQuery("FROM Student s " +
                    "WHERE s.lastName='Kowalska'").getResultList();

            List<Student> studentsFromDb2 = session.createQuery("FROM Student s " +
                    "WHERE s.lastName='Kowalska' OR s.firstName = 'Jacek'").getResultList();

            List<Student> studentsFromDbByEmailDomainContains = session.createQuery("FROM Student s " +
                    "WHERE s.email LIKE '%gmail%'").getResultList();

//            studentsFromDbByEmailDomainContains.forEach(System.out::println);
            session.getTransaction().commit();
        } finally {
            factory.close();
        }

    }
}
