package pl.marika.hibernate.demo.CRUD;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Student;
import pl.marika.hibernate.utils.DateUtils;

import java.text.ParseException;
import java.time.LocalDate;

/**
 * @author Marika Grzebieniowska on 22.07.2018
 * @project hibernate_tutorial
 */
public class CreateStudent {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml") // if name is default & file on classpath - you can leave it out
                .addAnnotatedClass(Student.class) // Spring provides a way to scan for them
                .buildSessionFactory();

        Session session;

        try {
            session = factory.getCurrentSession();

            String dobString = "2001/02/03";
            LocalDate dob = DateUtils.parseStringToLocalDate(dobString);
            Student tempStudent = new Student("Alicja", "Walczak", "aw@gmail.com", dob);
            session.beginTransaction();
            session.save(tempStudent);

            session.getTransaction().commit();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            factory.close();
        }
    }
}
