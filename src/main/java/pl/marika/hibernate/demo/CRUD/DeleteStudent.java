package pl.marika.hibernate.demo.CRUD;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Student;

/**
 * @author Marika Grzebieniowska on 23.07.2018
 * @project hibernate_tutorial
 */
public class DeleteStudent {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session;

        try {
            session = factory.getCurrentSession();
            session.beginTransaction();

            // Retrieving and deleting
            Student studentToDelete = session.get(Student.class, 1);
            session.delete(studentToDelete);

            // Deleting on-the-fly
            session.createQuery("DELETE FROM Student WHERE id=2")
                    .executeUpdate(); // this method can do an update or delete

            session.getTransaction().commit();
        } finally {
            factory.close();
        }
    }
}
