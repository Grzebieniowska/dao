package pl.marika.hibernate.demo.CRUD;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Student;

/**
 * @author Marika Grzebieniowska on 23.07.2018
 * @project hibernate_tutorial
 */
public class PrimaryKeyCreation {

    public static void main(String[] args) {


        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session;

        try {
            session = factory.getCurrentSession();

            Student tempStudent = new Student("Kasia", "Kowalska", "k.kowalska@fake.com");
            Student tempStudent2 = new Student("Lidia", "Malinowska", "malina123@gmail.com");
            Student tempStudent3 = new Student("Franciszek", "Górecki", "franciszek_górecki@.gorecki.pl");
            session.beginTransaction();
            session.save(tempStudent);
            session.save(tempStudent2);
            session.save(tempStudent3);
            session.getTransaction().commit();
        }
        finally {
            factory.close();
        }



    }

}
