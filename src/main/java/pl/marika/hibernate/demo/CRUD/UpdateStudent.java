package pl.marika.hibernate.demo.CRUD;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Student;


/**
 * @author Marika Grzebieniowska on 23.07.2018
 * @project hibernate_tutorial
 */
public class UpdateStudent {
    public static void main(String[] args) {


        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session;

        try {

            // updating one record
            session = factory.getCurrentSession();
            session.beginTransaction();

            Student studentInDb = session.get(Student.class, 1);
            studentInDb.setFirstName("Basia");

            session.getTransaction().commit();

            //updating all values by a criteria
            session = factory.getCurrentSession();
            session.beginTransaction();

            session.createQuery("UPDATE Student SET email='foo@gmail.com'").executeUpdate();

            session.getTransaction().commit();

        } finally {
            factory.close();
        }


    }
}
