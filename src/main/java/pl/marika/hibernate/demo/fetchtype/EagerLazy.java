package pl.marika.hibernate.demo.fetchtype;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import pl.marika.hibernate.entity.Course;
import pl.marika.hibernate.entity.Instructor;
import pl.marika.hibernate.entity.InstructorDetail;

import java.util.List;

/**
 * @author Marika Grzebieniowska on 24.07.2018
 * @project hibernate_tutorial
 */
public class EagerLazy {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        Session session = null;

        try {

            session = factory.getCurrentSession();
            session.beginTransaction();

            int instructorToFetch = 3;
            Instructor instructorFromDb = session.get(Instructor.class, instructorToFetch);

            // HOW TO GET COURSES IF THEY ARE LAZY:

            // 1. Call getter on courses explicitly
            List<Course> courses = instructorFromDb.getCourses();

            // 2. Run a HQL query that fetches courses while fetching instructor:
            Query<Instructor> query =
                    session.createQuery("SELECT i FROM Instructor i "
                                    + "JOIN FETCH i.courses "
                                    + "WHERE i.id=:theInstructorId",
                            Instructor.class);
            query.setParameter("theInstructorId", instructorToFetch);
            Instructor instructorFromDB = query.getSingleResult();

            session.getTransaction().commit();
            session.close();

            // this will wok now:
            instructorFromDB.getCourses().forEach(c -> System.out.println("Course title: " + c.getTitle()));


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
        }
    }

}


//            Solving LazyInitializationException
//            1. Call getter while session still open:
