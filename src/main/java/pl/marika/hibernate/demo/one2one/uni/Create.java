package pl.marika.hibernate.demo.one2one.uni;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Instructor;
import pl.marika.hibernate.entity.InstructorDetail;


/**
 * @author Marika Grzebieniowska on 24.07.2018
 * @project hibernate_tutorial
 */
public class Create {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();

        Session session;

        try {

            // create Instructor - my code
            session = factory.getCurrentSession();

            InstructorDetail tempInstructorDetail =
                    new InstructorDetail("http://www.youtube.com/channel/UC2coGyxf5", "skiing");
            Instructor tempInstructor =
                    new Instructor("Jack", "Black", "jb@gmail.com", tempInstructorDetail);

            session.beginTransaction();
            session.save(tempInstructor);
            session.getTransaction().commit();

            // create Instructor - udemy (using setter)
            session = factory.getCurrentSession();

            Instructor tempInstructor2 =
                    new Instructor("Anna", "Martinez", "am@gmail.com");
            InstructorDetail tempInstructorDetail2 =
                    new InstructorDetail("http://www.youtube.com/channel/hkix76it4cl", "jogging");
            tempInstructor2.setInstructorDetail(tempInstructorDetail2);

            Instructor tempInstructor3 =
                    new Instructor("Linus", "Torvalds", "linusss@gmail.com");
            InstructorDetail tempInstructorDetail3 =
                    new InstructorDetail("http://www.youtube.com/channel/hkix76it4cl", "sleeping");
            tempInstructor3.setInstructorDetail(tempInstructorDetail3);

            session.beginTransaction();
            session.save(tempInstructor2); // also saves InstructorDetails in the other table
            session.save(tempInstructor3);
            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            factory.close();
        }
    }
}
