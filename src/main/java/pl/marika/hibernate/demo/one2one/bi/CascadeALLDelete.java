package pl.marika.hibernate.demo.one2one.bi;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Instructor;
import pl.marika.hibernate.entity.InstructorDetail;

/**
 * @author Marika Grzebieniowska on 24.07.2018
 * @project hibernate_tutorial
 */

//instructorDetail in Instructor should be
// @OneToOne(cascade = CascadeType.ALL)
public class CascadeALLDelete {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();

        Session session = null;

        try {

            session = factory.getCurrentSession();
            session.beginTransaction();

            InstructorDetail tempInstructorDetail = session.get(InstructorDetail.class, 1);
            Instructor retrievedInstructor = tempInstructorDetail.getInstructor();
            session.delete(retrievedInstructor);

            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
        }
    }
}
