package pl.marika.hibernate.demo.one2one.uni;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Instructor;
import pl.marika.hibernate.entity.InstructorDetail;

/**
 * @author Marika Grzebieniowska on 24.07.2018
 * @project hibernate_tutorial
 */
public class Delete {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();

        Session session;

        try {

            session = factory.getCurrentSession();
            session.beginTransaction();

            Instructor tempInstructor = session.get(Instructor.class, 5);
            if (tempInstructor != null) {
                // also deletes his details from 2nd table
                // because CascadeType.ALL in Instructor class
                session.delete(tempInstructor);
            }

            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            factory.close();
        }
    }


}
