package pl.marika.hibernate.demo.one2one.bi;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Instructor;
import pl.marika.hibernate.entity.InstructorDetail;

/**
 * @author Marika Grzebieniowska on 24.07.2018
 * @project hibernate_tutorial
 */

// field instructor in InstructorDetail must be
// @OneToOne(cascade = {
//        CascadeType.DETACH,
//        CascadeType.MERGE,
//        CascadeType.PERSIST,
//        CascadeType.REFRESH})
//
// field instructorDetail in Instructor remains
// @OneToOne(cascade = CascadeType.ALL)
public class CascadePartialDelete {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();

        Session session = null;

        try {

            session = factory.getCurrentSession();
            session.beginTransaction();

            InstructorDetail tempInstructorDetail = session.get(InstructorDetail.class, 12);
            Instructor tempInstructor = tempInstructorDetail.getInstructor();

            tempInstructor.setInstructorDetail(null);
            session.delete(tempInstructorDetail);

            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
        }
    }


}
