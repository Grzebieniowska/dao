package pl.marika.hibernate.demo.one2many.uni;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Course;
import pl.marika.hibernate.entity.Instructor;
import pl.marika.hibernate.entity.InstructorDetail;
import pl.marika.hibernate.entity.Review;

import java.util.List;

/**
 * @author Marika Grzebieniowska on 24.07.2018
 * @project hibernate_tutorial
 */
public class CRUD_one2manyUni {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        Session session = null;

        try {

            // CREATE -------------------------------
            // Add course with reviews, CascadeType.ALL will cause save()
            // to also save reviews while saving the course
            session = factory.getCurrentSession();
            session.beginTransaction();

            Course tempCourse = new Course("Horse riding");
            Review tempReview = new Review("Very gooood course");
            Review tempReview2 = new Review("NO NO NO");
            Review tempReview3 = new Review("Easy to follow. NOT");

            tempCourse.addReview(tempReview);
            tempCourse.addReview(tempReview2);
            tempCourse.addReview(tempReview3);

            session.save(tempCourse);
            session.getTransaction().commit();

            // READ -------------------------------
            session = factory.getCurrentSession();
            session.beginTransaction();

            Course courseFromDb = session.get(Course.class, 1);
            List<Review> reviews = courseFromDb.getReviews();

            // ......bla bla bla

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
        }
    }
}
