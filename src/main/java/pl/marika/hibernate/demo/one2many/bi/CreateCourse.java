package pl.marika.hibernate.demo.one2many.bi;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Course;
import pl.marika.hibernate.entity.Instructor;
import pl.marika.hibernate.entity.InstructorDetail;

/**
 * @author Marika Grzebieniowska on 24.07.2018
 * @project hibernate_tutorial
 */
public class CreateCourse {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        Session session = null;

        try {

            session = factory.getCurrentSession();
            session.beginTransaction();

            Instructor tempInstructor = session.get(Instructor.class, 13);
            Course tempCourse1 = new Course("Baśnie");
            Course tempCourse2 = new Course("Geografia");

            // set-up bi-directional line (update all classes with info about each other)
            tempInstructor.add(tempCourse1);
            tempInstructor.add(tempCourse2);

            // we must do this save, because these objects are brand new
            // and not yet persistent (not in DB and not managed by Hibernate)
            session.save(tempCourse1);
            session.save(tempCourse2);

            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
        }
    }
}
