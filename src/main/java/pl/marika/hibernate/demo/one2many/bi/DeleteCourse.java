package pl.marika.hibernate.demo.one2many.bi;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.Course;
import pl.marika.hibernate.entity.Instructor;
import pl.marika.hibernate.entity.InstructorDetail;

import java.util.List;

/**
 * @author Marika Grzebieniowska on 24.07.2018
 * @project hibernate_tutorial
 */
public class DeleteCourse {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        Session session = null;

        try {

            session = factory.getCurrentSession();
            session.beginTransaction();

            Instructor instructor = session.get(Instructor.class, 1);
            List<Course> courses = instructor.getCourses();
            Course courseToDelete = courses.get(1);
            session.remove(courseToDelete);

            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
        }
    }
}
