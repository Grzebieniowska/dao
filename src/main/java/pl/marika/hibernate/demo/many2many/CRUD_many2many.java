package pl.marika.hibernate.demo.many2many;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.*;
import pl.marika.hibernate.utils.DateUtils;
import pl.marika.testdata.Runner;

import java.text.ParseException;

/**
 * @author Marika Grzebieniowska on 25.07.2018
 * @project hibernate_tutorial
 */
public class CRUD_many2many {

    public static void main(String[] args) {

        Runner.clearTables();

        SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();
        try {
            Session session = factory.getCurrentSession();
            try {

                // CREATE

                session.beginTransaction();

                Course course = new Course("Knitting");
                session.save(course);

                Student student1 = new Student("Marika", "Grzebieniowska", "mg@gmail.com",
                        DateUtils.parseStringToLocalDate("1982/11/01"));
                Student student2 = new Student("Tomasz", "Stomasz", "tomek@gmail.com",
                        DateUtils.parseStringToLocalDate("2001/03/22"));
                course.addStudent(student1);
                course.addStudent(student2);
                session.save(student1);
                session.save(student2);

                session.getTransaction().commit();

                // UPDATE

                session = factory.getCurrentSession();
                session.beginTransaction();

                Course course2 = new Course("Schooting");
                Student marika = session.get(Student.class, 1);
                course2.addStudent(marika);
                session.save(course2);

                session.getTransaction().commit();

                // READ

                session = factory.getCurrentSession();
                session.beginTransaction();

                marika = session.get(Student.class, 1);
                marika.getCourses().forEach(System.out::println);

                session.getTransaction().commit();

                // DELETE Student - should not delete any of his courses

                session = factory.getCurrentSession();
                session.beginTransaction();

                Student studentToDelete = session.get(Student.class, 1);
                session.remove(studentToDelete);

                session.getTransaction().commit();

                // DELETE Course - should not delete the enlisted students

                session = factory.getCurrentSession();
                session.beginTransaction();

                Course courseToDelete = session.get(Course.class, 1);
                session.remove(courseToDelete);

                session.getTransaction().commit();

            } catch (ParseException e) {
                e.printStackTrace();
            } finally {
                session.close();
            }
        } finally {
            factory.close();
        }
    }
}


