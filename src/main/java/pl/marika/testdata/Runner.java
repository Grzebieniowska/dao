package pl.marika.testdata;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.marika.hibernate.entity.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Marika Grzebieniowska on 22.07.2018
 * @project hibernate_tutorial
 */
public class Runner {

    public static void main(String[] args) {

        testDbConnection();
        clearTables();
//        populateWithData();

    }

    public static void testDbConnection() {
        String jdbcUrl = "jdbc:mysql://localhost:3306/hibernateclass?useSSL=false&serverTimezone=Poland";
        String user = "hbstudent";
        String pass = "hbstudent";

        try {
            System.out.println("Connecting to DB: " + jdbcUrl);
            Connection conn = DriverManager.getConnection(jdbcUrl, user, pass);
            System.out.println("Connection successful!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void populateWithData() {

        final Random random = new Random();

        try (SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();
             Session session = factory.getCurrentSession()) {

            session.beginTransaction();

            // create instructors
            ArrayList<Instructor> instructors = new ArrayList<>();
            instructors.add(new Instructor("Marek", "Goliński", "mg@gmail.com",
                    new InstructorDetail("http://youtube/hco4q87tcjyn7", "dancing")));
            instructors.add(new Instructor("Andrzej", "Koc", "a.koc@o2.pl",
                    new InstructorDetail("http://youtube/03kcjxnk2", "hunting")));
            instructors.add(new Instructor("Barbara", "Stasińska", "basia95@yahoo.com",
                    new InstructorDetail("http://youtube/ick48kj82tc", "chess")));
            instructors.add(new Instructor("Robert", "Malinowski", "robert_malinowski@wp.pl",
                    new InstructorDetail("http://youtube/j4ok8t4cnn", "swimming")));
            instructors.add(new Instructor("Peter", "Schmidt", "p.schmidt@gmx.de",
                    new InstructorDetail("http://youtube/cnfg7t2610", "PC gaming")));

            // create courses
            ArrayList<Course> courses = new ArrayList<>();
            courses.add(new Course("Mathematics"));
            courses.add(new Course("Geography"));
            courses.add(new Course("Quantum Physics"));
            courses.add(new Course("Theology"));
            courses.add(new Course("English literature"));
            courses.add(new Course("Organic chemistry"));
            courses.add(new Course("Cellular biology"));
            courses.add(new Course("Renaissance Art"));
            courses.add(new Course("Dance"));
            courses.add(new Course("Psychology"));

            // distribute courses among Instructors
            int courseLimitForInstructor;
            List<String> availableCoursesTitles = new ArrayList<>();
            courses.forEach(c -> availableCoursesTitles.add(c.getTitle()));
            courseLimitForInstructor = Math.max(1, courses.size() / instructors.size()); // fair distribution

            for (Instructor instructor : instructors) {
                instructor.setCourses(new ArrayList<>());
                while (instructor.getCourses().size() < courseLimitForInstructor && availableCoursesTitles.size() > 0) {
                    int randomIndex = random.nextInt(courses.size());
                    Course randomCourse = courses.get(randomIndex);

                    if (availableCoursesTitles.contains(randomCourse.getTitle())) {
                        int indexOf = availableCoursesTitles.indexOf(randomCourse.getTitle());
                        instructor.add(randomCourse);
                        availableCoursesTitles.remove(indexOf);
                    }
                }
            }

            // create reviews
            ArrayList<Review> reviews = new ArrayList<>();
            reviews.add(new Review("Fantastic! Best course on udemy"));
            reviews.add(new Review("No comprende"));
            reviews.add(new Review("Too fast, I didn't understand anything"));
            reviews.add(new Review("8/10, good content although some is out of date"));
            reviews.add(new Review("I'm happy with this course"));
            reviews.add(new Review("Good for experienced programmers, some concepts were not clear to me"));
            reviews.add(new Review("Too long and too slow, 5/10"));
            reviews.add(new Review("The best course so far"));
            reviews.add(new Review("I hope they will make na update sonn, 7/10."));
            reviews.add(new Review("Utter garbage"));

            // add reviews to courses
            Course currentCourse = courses.get(0);
            currentCourse.addReview(reviews.get(0));
            currentCourse.addReview(reviews.get(1));
            currentCourse.addReview(reviews.get(2));
            currentCourse = courses.get(1);
            currentCourse.addReview(reviews.get(3));
            currentCourse.addReview(reviews.get(4));
            currentCourse = courses.get(2);
            currentCourse.addReview(reviews.get(5));
            currentCourse.addReview(reviews.get(6));
            currentCourse.addReview(reviews.get(7));
            currentCourse = courses.get(3);
            currentCourse.addReview(reviews.get(8));
            currentCourse = courses.get(4);
            currentCourse.addReview(reviews.get(9));

            // save all entities to DB
            instructors.forEach(i -> session.save(i));
            courses.forEach(c -> session.save(c));
            reviews.forEach(r -> session.save(r));

            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static void clearTables() {

        try (SessionFactory factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();
             Session session = factory.getCurrentSession()) {

            session.beginTransaction();

            // drop all tables - runs in MySQL, I don't know how to do it here + then create tables anew?

            /*
             SELECT CONCAT( "DROP TABLE ", GROUP_CONCAT(TABLE_NAME) ) INTO @dropcmd
             FROM information_schema.TABLES WHERE TABLE_SCHEMA = "hibernateclass";

             PREPARE str FROM @dropcmd;
             EXECUTE str;
             DEALLOCATE PREPARE str;
             */

            // create tables from my sql script (also not done here)

            // clear all tables
            session.createSQLQuery("SET FOREIGN_KEY_CHECKS=0").executeUpdate();
            session.createSQLQuery("TRUNCATE instructor").executeUpdate();
            session.createSQLQuery("TRUNCATE instructor_detail").executeUpdate();
            session.createSQLQuery("TRUNCATE course").executeUpdate();
            session.createSQLQuery("TRUNCATE review").executeUpdate();
            session.createSQLQuery("TRUNCATE course_student").executeUpdate();
            session.createSQLQuery("TRUNCATE student").executeUpdate();
            session.createSQLQuery("SET FOREIGN_KEY_CHECKS=1");

            session.getTransaction().commit();
        }
    }
}





